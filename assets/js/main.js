$(document).ready(function() {
 
	// For the Second level Dropdown menu, highlight the parent	
	$( ".dropdown-menu" )
	.mouseenter(function() {
		$(this).parent('li').addClass('active');
	})
	.mouseleave(function() {
		$(this).parent('li').removeClass('active');
	});
 
	$('input[data-plugin="switchery"]').each(function() {
		var options = {
			// color: '',
			size: $(this).attr('data-size')	
		};
		new Switchery(this, options);
	});
	
});